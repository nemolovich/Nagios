# !/bin/bash

mode="$1"

# Use `export NRPE_SSL_LIB_DIR="/usr/lib/x86_64-redhat-linux6E"` for CentOS and RedHat for example
SSL_LIB_DIR=${NRPE_SSL_LIB_DIR:-/usr/lib/x86_64-linux-gnu}
NAGIOS_VERSION=${NAGIOS_VERSION:-4.3.3}
NAGIOS_HOME=${NAGIOS_HOME:-/usr/local/nagios}
NAGIOS_PLUGINS_VERSION=${NAGIOS_PLUGINS_VERSION:-2.2.1}
NAGIOS_NRPE_VERSION=${NAGIOS_NRPE_VERSION:-3.2.0}
NAGIOS_PASSWORD=${NAGIOS_PASSWORD:-nagios01}
APACHE_CONFIGS=${APACHE_CONFIGS:-/etc/apache2/sites-enabled}
NRPE_PORT=${NRPE_PORT:-4888}

INSTALL_TYPE_CONTAINER="container"
INSTALL_TYPE_HOST="host"

[ ! -z "${mode}" -a "${mode}" = ${INSTALL_TYPE_CONTAINER} ] && INSTALL_TYPE=${INSTALL_TYPE_CONTAINER} || INSTALL_TYPE=${INSTALL_TYPE_HOST}

echo "Installing on ${INSTALL_TYPE}"

if ! cat /etc/passwd | grep -qe nagios ; then
	echo "Adding user nagios..."
	useradd -m nagios
	echo "${NAGIOS_PASSWORD}
${NAGIOS_PASSWORD}" | passwd nagios
else
	echo "User nagios already exists"
fi

if ! cat /etc/group | grep -qe nagcmd ; then
	echo "Adding group nagcmd..."
	groupadd nagcmd
else
	echo "Group nagcmd already exists"
fi
if ! cat /etc/group | grep nagcmd | grep -qe nagios ; then
	echo "Adding user nagios to group nagcmd..."
	usermod -a -G nagcmd nagios
else
	echo "User nagios is already in group nagcmd"
fi
if [ ${INSTALL_TYPE} = ${INSTALL_TYPE_CONTAINER} ] ;then
	if ! cat /etc/group | grep nagcmd | grep -qe www-data ; then
		echo "Adding user www-data to group nagcmd..."
		usermod -a -G nagcmd www-data
	else
		echo "User www-data is already in group nagcmd"
	fi
fi

cd /opt
wget --no-check-certificate https://sourceforge.net/projects/nagios/files/nagios-4.x/nagios-${NAGIOS_VERSION}/nagios-${NAGIOS_VERSION}.tar.gz
wget --no-check-certificate https://sourceforge.net/projects/nagios/files/nrpe-3.x/nrpe-${NAGIOS_NRPE_VERSION}/nrpe-${NAGIOS_NRPE_VERSION}.tar.gz
wget http://www.nagios-plugins.org/download/nagios-plugins-${NAGIOS_PLUGINS_VERSION}.tar.gz

tar -xzf nagios-${NAGIOS_VERSION}.tar.gz
tar -xzf nrpe-${NAGIOS_NRPE_VERSION}.tar.gz
tar -xzf nagios-plugins-${NAGIOS_PLUGINS_VERSION}.tar.gz

cd /opt/nagios-${NAGIOS_VERSION}
echo "Configure Nagios with:
	--with-command-group=nagcmd
"
./configure --with-command-group=nagcmd
make all
make install
make install-init
make install-config
make install-commandmode
make install-webconf

cd /opt/nrpe-${NAGIOS_NRPE_VERSION}
echo "Configure NRPE with:
	--with-ssl=/usr/bin/openssl
	--with-ssl-lib=${SSL_LIB_DIR}
	--enable-command-args
	--with-nagios-user=nagios
	--with-nagios-group=nagios
	--with-nrpe-port=${NRPE_PORT}
"
./configure --with-ssl=/usr/bin/openssl --with-ssl-lib=${SSL_LIB_DIR} \
--enable-command-args --with-nagios-user=nagios \
--with-nagios-group=nagios --with-nrpe-port=${NRPE_PORT}
if [ ${INSTALL_TYPE} = ${INSTALL_TYPE_CONTAINER} ] ; then
	echo "Make for container"
	make check_nrpe
	make install
else
	echo "Make for host"
	make all
	make install
	make install-plugin
	make install-daemon-config
	make install-daemon
	mkdir -p /etc/xinetd.d/
	make install-xinetd
	cp $(cd `dirname $0`;pwd)/host/nrpe.cfg ${NAGIOS_HOME}/etc/
fi

cd /opt/nagios-plugins-${NAGIOS_PLUGINS_VERSION}
./configure --with-nagios-user=nagios --with-nagios-group=nagios
echo "Configure NRPE with:
	--with-nagios-user=nagios
	--with-nagios-group=nagios
"
make
make install

htpasswd_file=$(dirname ${APACHE_CONFIGS})/htpasswd.users
[ ${INSTALL_TYPE} = ${INSTALL_TYPE_CONTAINER} ] && echo "Create nagios credential on ${htpasswd_file}" && htpasswd -cb ${htpasswd_file} nagios ${NAGIOS_PASSWORD}


echo "Remove temporary folders..."
rm -rf /opt/nagios-${NAGIOS_VERSION} /opt/nrpe-${NAGIOS_NRPE_VERSION} /opt/nagios-plugins-${NAGIOS_PLUGINS_VERSION}
rm -f /opt/nagios-${NAGIOS_VERSION}.tar.gz /opt/nrpe-${NAGIOS_NRPE_VERSION}.tar.gz /opt/nagios-plugins-${NAGIOS_PLUGINS_VERSION}.tar.gz

exit 0
