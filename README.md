# Nagios Image

Nagios Docker container using NRPE module to monitor the host machine.

Versions:
 * Nagios: **4.3.3**
 * Nagios plugins: **2.2.1**
 * NRPE Plugin: **3.2.0**

## Host configuration

On the Host machine you have to install nagios as remote client. You can start the script 
[install_nagios.sh](./install_nagios.sh) from your host to install all needed modules.

To use NRPE plugin from the container you have to listen on the configured NRPE port
(value set to **4888** for this image) via `xinetd` service. Then modify your `/etc/services`
file to allow port by adding following line:
```
nrpe            4888/tcp                # NRPE for Nagios
```

## Build Image
Build image from:
```sh
docker build -t capgemininantes/nagios ./
```

## Run Container
Run image with:
```sh
docker run --name nagios_web \
  -v /opt/volumes/nagios/apache.conf:/etc/apache2/sites-enabled/nagios.conf:Z \
  -v /opt/volumes/nagios/etc:/usr/local/nagios/etc/:Z -e NAGIOS_PORT=80 \
  -e NAGIOS_PASSWORD=nagiospass --network host capgemininantes/nagios
```

Don't forget to use `--network=host` to use NRPE module.

The HTTPD configuration use unix htpasswd file for access on Nagios Web page by default. The `nagios` user
is added in the default htpasswd and `cgi.cfg` file. In order to add an new user you have to edit these both
files (htpasswd and `cgi.cfg`).

eg:
Adding myuser/mypassword into htpasswd file (docker container volume need to be configured):
```
htpasswd -cb /path/to/volume/htpasswd_file myuser mypassword
```

File `cgi.cfg` adding `myuser` user:
```
[...]

authorized_for_system_information=nagiosadmin,nagios,myuser
authorized_for_configuration_information=nagiosadmin,nagios,myuser
authorized_for_system_commands=nagiosadmin,nagios,myuser
authorized_for_all_services=nagiosadmin,nagios,myuser
authorized_for_all_hosts=nagiosadmin,nagios,myuser
authorized_for_all_service_commands=nagiosadmin,nagios,myuser
authorized_for_all_host_commands=nagiosadmin,nagios,myuser
```

### --env|-e

| **Environment Variable Name** | **Default Value** | **Description**                             |
|------------------------------:|:------------------|:--------------------------------------------|
| **NAGIOS_PASSWORD**           | "nagios01"        | The password for uniox user `nagios`        |
| **NRPE_PORT**                 | `4888`            | Communication port for NRPE (xinetd)        |
| **NAGIOS_PORT**               | `4880`            | HTTP port for Nagios                        |
| **NAGIOS_HTTPS_PORT**         | `4881`            | HTTPS port for Nagios (not used by default) |

### --volume|-v

| **Container path**                       | **Description**                           |
|:-----------------------------------------|:------------------------------------------|
| `/usr/local/nagios/etc`                  | Nagios configuration files directory      |
| `/etc/apache2/sites-enabled/nagios.conf` | HTTPD file configuration for Nagios       |
| `/etc/apache2/htpasswd.users`            | Default htpasswd file with allowed users  |


