FROM ubuntu:17.10
LABEL maintainer="Brian GOHIER<brian.gohier@capgemini.com>"

RUN apt-get update \
 && apt-get install -y --no-install-recommends \
  openssl \
  libssl-dev \
  libcurl4-openssl-dev \
  unzip \
  wget \
  libgd2-xpm-dev \
  build-essential \
  net-tools \
  apache2 \
  php7.1-gd \
  php7.1-ldap \
  php-gd \
  libapache2-mod-php7.1 \
  libapache2-mod-nss \
  iputils-ping \
  && apt-get -y clean && apt-get -y autoremove \
  && rm -rf /var/lib/apt/lists/* && rm -rf /usr/share/doc


ENV APACHE_CONFIGS="/etc/apache2/sites-enabled"
ENV NAGIOS_HOME="/usr/local/nagios"
ENV NAGIOS_LIBEXEC="${NAGIOS_HOME}/libexec"
ENV NAGIOS_ETC="${NAGIOS_HOME}/etc"
ENV BACKUP_ETC="/root/nagios_etc"
ENV BACKUP_APACHE="/root/apache_conf"
ENV NAGIOS_VERSION="4.3.3"
ENV NAGIOS_PLUGINS_VERSION="2.2.1"
ENV NAGIOS_NRPE_VERSION="3.2.0"
ENV NAGIOS_PASSWORD="nagios01"
ENV NRPE_PORT=4888
ENV NAGIOS_PORT=4880
ENV NAGIOS_HTTPS_PORT=4881

RUN sed -i.bak '1 i ServerName nagios_web\n' ${APACHE_CONFIGS}/000-default.conf
COPY install_nagios.sh /root/install_nagios.sh

RUN chmod +x /root/install_nagios.sh && chmod 777 /run/ && /root/install_nagios.sh "container"
COPY ./conf.d/* ${APACHE_CONFIGS}/
COPY ./cfg/*.cfg ${NAGIOS_ETC}/
COPY ./cfg/objects/*.cfg ${NAGIOS_ETC}/objects/

RUN a2enmod cgi authnz_ldap
RUN mkdir ${BACKUP_ETC} && cp -rp ${NAGIOS_ETC}/* ${BACKUP_ETC}/ && \
  mkdir ${BACKUP_APACHE} && cp -rp ${APACHE_CONFIGS}/nagios.conf ${BACKUP_APACHE}/

COPY start_nagios.sh /root/start_nagios.sh

VOLUME ${NAGIOS_ETC}

EXPOSE ${NAGIOS_PORT}
EXPOSE ${NAGIOS_HTTPS_PORT}
STOPSIGNAL SIGTERM

RUN chmod +x /root/start_nagios.sh
CMD ["/root/start_nagios.sh"]
