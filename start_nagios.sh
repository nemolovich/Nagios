#!/bin/bash

echo "NAGIOS is now starting..."

wait_cmd()
{
	service nagios restart
	service apache2 restart
	while [ -f /usr/local/nagios/var/nagios.lock ] ; do sleep 1 ; done
}

if ! cat /etc/hosts | grep -qe nagios_web ; then 
	echo "`ifconfig | grep eth0 -A 1 | grep inet | awk '{print $2}'` nagios_web" >> /etc/hosts
fi

if [ -z "$(ls ${NAGIOS_ETC})" ] ; then
	echo "Restore NAGIOS backup configuration..."
	cp -rp ${BACKUP_ETC}/* ${NAGIOS_ETC}/
fi

if [ -z "$(cat ${APACHE_CONFIGS}/nagios.conf)" ] ; then
	echo "Restore HTTPD backup configuration..."
	cp -rp ${BACKUP_APACHE}/nagios.conf ${APACHE_CONFIGS}/
fi

echo "Check current ports..."
echo "File '${APACHE_CONFIGS}/000-default.conf'..."
if cat ${APACHE_CONFIGS}/000-default.conf | grep -qe ":${NAGIOS_PORT}>" ; then
	echo "HTTP port used ${NAGIOS_PORT}"
else
	backup_file=${APACHE_CONFIGS}/000-default.conf.$(date "+%Y%m%d%H%M%S")
	echo "Backup to '${backup_file}'"
	cp ${APACHE_CONFIGS}/000-default.conf ${backup_file}
	echo "Update HTTP port to ${NAGIOS_PORT}"
	sed -i 's/<VirtualHost \*:[0-9]\+>/<VirtualHost \*:'${NAGIOS_PORT}'>/g' ${APACHE_CONFIGS}/000-default.conf
fi

echo "File '$(dirname ${APACHE_CONFIGS})/ports.conf'..."
bkuped=false
if cat $(dirname ${APACHE_CONFIGS})/ports.conf | grep -qe "^Listen ${NAGIOS_PORT}$" ; then
	echo "HTTP port used ${NAGIOS_PORT}"
else
	backup_file=$(dirname ${APACHE_CONFIGS})/ports.conf.$(date "+%Y%m%d%H%M%S")
	echo "Backup to '${backup_file}'"
	cp $(dirname ${APACHE_CONFIGS})/ports.conf ${backup_file}
	bkuped=true
	echo "Update HTTP port to ${NAGIOS_PORT}"
	sed -i 's/^Listen [0-9]\+/Listen '${NAGIOS_PORT}'/g' $(dirname ${APACHE_CONFIGS})/ports.conf
fi
if cat $(dirname ${APACHE_CONFIGS})/ports.conf | grep -qe "^[[:space:]]Listen ${NAGIOS_HTTPS_PORT}$" ; then
	echo "HTTPS port used ${NAGIOS_HTTPS_PORT}"
else
	echo "Update HTTPS port to ${NAGIOS_HTTPS_PORT}"
	if ! ${bkuped} ; then
		backup_file=$(dirname ${APACHE_CONFIGS})/ports.conf.$(date "+%Y%m%d%H%M%S")
		echo "Backup to '${backup_file}'"
		cp $(dirname ${APACHE_CONFIGS})/ports.conf ${backup_file}
	fi
	sed -i 's/^\([[:space:]]\)Listen [0-9]\+/\1Listen '${NAGIOS_HTTPS_PORT}'/g' $(dirname ${APACHE_CONFIGS})/ports.conf
fi

bkuped=false
echo "File '${NAGIOS_ETC}/objects/container.cfg'..."
if cat ${NAGIOS_ETC}/objects/container.cfg | grep -qe "check_http\!${NAGIOS_PORT}" ; then
	echo "HTTP port used ${NAGIOS_PORT}"
else
	echo "Update HTTP port to ${NAGIOS_PORT}"
	backup_file=${NAGIOS_ETC}/objects/container.cfg.$(date "+%Y%m%d%H%M%S")
	echo "Backup to '${backup_file}'"
	cp ${NAGIOS_ETC}/objects/container.cfg ${backup_file}
	bkuped=true
	sed -i ':a;N;$!ba;s/\(service_description[[:space:]]\+HTTP on \)[0-9]\+\(\n[[:space:]]\+check_command[[:space:]]\+check_http\!\)[0-9]\+/\1'${NAGIOS_PORT}'\2'${NAGIOS_PORT}'/' ${NAGIOS_ETC}/objects/container.cfg
fi
if cat ${NAGIOS_ETC}/objects/container.cfg | grep -qe "check_http\!${NAGIOS_HTTPS_PORT}" ; then
	echo "HTTPS port used ${NAGIOS_HTTPS_PORT}"
else
	echo "Update HTTPS port to ${NAGIOS_HTTPS_PORT}"
	if ! ${bkuped} ; then
		backup_file=${NAGIOS_ETC}/objects/container.cfg.$(date "+%Y%m%d%H%M%S")
		echo "Backup to '${backup_file}'"
		cp ${NAGIOS_ETC}/objects/container.cfg ${backup_file}
	fi
	sed -i ':a;N;$!ba;s/\(service_description[[:space:]]\+HTTPS on \)[0-9]\+\(\n[[:space:]]\+check_command[[:space:]]\+check_http\!\)[0-9]\+/\1'${NAGIOS_HTTPS_PORT}'\2'${NAGIOS_HTTPS_PORT}'/' ${NAGIOS_ETC}/objects/container.cfg
fi

echo "File '${NAGIOS_ETC}/objects/localhost.cfg'..."
if cat ${NAGIOS_ETC}/objects/localhost.cfg | grep -qe "check_http\!${NAGIOS_PORT}" ; then
	echo "HTTP port used ${NAGIOS_PORT}"
else
	echo "Update HTTP port to ${NAGIOS_PORT}"
	backup_file=${NAGIOS_ETC}/objects/localhost.cfg.$(date "+%Y%m%d%H%M%S")
	echo "Backup to '${backup_file}'"
	cp ${NAGIOS_ETC}/objects/localhost.cfg ${backup_file}
	bkuped=true
	sed -i ':a;N;$!ba;s/\(service_description[[:space:]]\+HTTP on \)[0-9]\+\(\n[[:space:]]\+check_command[[:space:]]\+check_http\!\)[0-9]\+/\1'${NAGIOS_PORT}'\2'${NAGIOS_PORT}'/' ${NAGIOS_ETC}/objects/localhost.cfg
fi
if cat ${NAGIOS_ETC}/objects/localhost.cfg | grep -qe "check_http\!${NAGIOS_HTTPS_PORT}" ; then
	echo "HTTPS port used ${NAGIOS_HTTPS_PORT}"
else
	echo "Update HTTPS port to ${NAGIOS_HTTPS_PORT}"
	if ! ${bkuped} ; then
		backup_file=${NAGIOS_ETC}/objects/localhost.cfg.$(date "+%Y%m%d%H%M%S")
		echo "Backup to '${backup_file}'"
		cp ${NAGIOS_ETC}/objects/localhost.cfg ${backup_file}
	fi
	sed -i ':a;N;$!ba;s/\(service_description[[:space:]]\+HTTPS on \)[0-9]\+\(\n[[:space:]]\+check_command[[:space:]]\+check_http\!\)[0-9]\+/\1'${NAGIOS_HTTPS_PORT}'\2'${NAGIOS_HTTPS_PORT}'/' ${NAGIOS_ETC}/objects/localhost.cfg
fi

htpasswd_file=$(dirname ${APACHE_CONFIGS})/htpasswd.users
tmp=$(htpasswd -vb ${htpasswd_file} nagios ${NAGIOS_PASSWORD})
passwd_ok=$?

if [ ${passwd_ok} -ne 0 ] ; then
	if [ ${passwd_ok} -eq 6 ] ; then
		echo "Create nagios credential on ${htpasswd_file}"
		htpasswd -cb ${htpasswd_file} nagios ${NAGIOS_PASSWORD}
	else
		echo "Update nagios password on ${htpasswd_file}"
		htpasswd -b ${htpasswd_file} nagios ${NAGIOS_PASSWORD}
	fi
	echo "${NAGIOS_PASSWORD}
	${NAGIOS_PASSWORD}" | passwd nagios
fi

wait_cmd &
pid="$!"
trap "echo 'Stopping NAGIOS'; service apache2 stop; service nagios stop;" SIGINT SIGTERM

while kill -0 ${pid} > /dev/null 2>&1; do
	wait
done
